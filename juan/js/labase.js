
var BASE = {}; // definir un Objeto tal como new Object();, pero con JSON

BASE.mostrar = function(msg){ // aqui definimos un metodo para mostrar un mensaje
  // aqui obtenemos el elemento HTML contenedor
  // que podemos buscar en google con " w3 dom " 
  var elContenedor  = document.querySelector('#contenedor');  
  
  // en este caso, teniendo el puntero al elemento, la variable, le agrego
  // dentro el contenido con u n fin de linhea de HTML
  // este atributo innerHTML, lo busco en la w3 y 
  // encuentro los otros 5000 atributes que necesite
  elContenedor.innerHTML += msg + '<br/>';
}; // este punto y coma, es porque la funcion se definio como contenido de un atributo.


BASE.LABASE_INSTANCE = null; // Singleton: Unica instancia del objeto Base de Datos.
BASE.crearBase = function(){ // 
    BASE.mostrar('Creando la Base...');
    // BASE.mostrar(" 2 == '2' " +  (2 == '2') );
    // BASE.mostrar(" 2 === '2' " +  (2 === '2') );
    
    if( BASE.LABASE_INSTANCE === null ){ // https://en.wikipedia.org/wiki/Singleton_pattern
      BASE.LABASE_INSTANCE = {}; // Existe y no es nulo!!
      BASE.mostrar('Creando la Base...[OK]');
      alasql(" CREATE TABLE productos (prod_id INT, prod_nombre VARCHAR(200), prod_precio FLOAT);");
    }
};

BASE.insertar = function(){
  var laEntrada =  document.querySelector('#entrada');  
  // Aca logre cargar el dato de entrada en una variable

  // rfc, w3, ansi, ecma estandares
  // Aca iria lo que yo quiero hacer con la Base de Datos
  // el INSERT
  // entonces:
  /*
   ANSI SQL:
   7 palabras claves: Usuario y Roles, GRANT Permisos
   
   *******************
   DDL: Modifica la estrucutrad de la tabla
   CREATE, 
   DROP, 
   ALTER
   
   DML: Manipula los datos dentro de las tablas
   INSERT, 
   UPDATE, 
   DELETE
   
   DQL: busquedas, lecturas, cuentas.
   SELECT
   *******************
   INT
   LONG
   FLOAT
   DOUBLE
   CHAR
   VARCHAR(255)
   DATE, TIME, DATETIME, TIMESTAMP
   
  */

  // Aca quiero motrar los datos con la funcion de muestra.
  BASE.mostrar( laEntrada.value );
  BASE.contadorMentiroso++;
   alasql(" INSERT INTO productos (prod_id , prod_nombre , prod_precio ) VALUES ("+ BASE.contadorMentiroso +" ,'" + laEntrada.value + "', 1000);");

};
BASE.consultar = function(){
    var respuestaSQL = alasql(" SELECT * FROM productos ");
    
//    BASE.mostrar( JSON.stringify( respuestaSQL ) );
    

//    var data = BASE.mostrar( JSON.stringify( respuestaSQL ) );
    
    for (i = 0; i < respuestaSQL.length; i++) {
    //BASE.mostrar(respuestaSQL[i].prod_id + "\t" + respuestaSQL[i].prod_nombre + "\t" + respuestaSQL[i].prod_precio);
    BASE.mostrar("<tr>"+respuestaSQL[i].prod_id + "\t" + respuestaSQL[i].prod_nombre + "\t" + respuestaSQL[i].prod_precio+"</tr>" + "<input type='button' value='Borrar' id='btnBorrar' />");
    var botonBorrar = document.querySelector('#btnBorrar');
    botonBorrar.setAttribute('onclick','BASE.borrar(respuestaSQL[i].prod_id);');
        
    }
    
BASE.borrar = function(id){
    alasql(" DELETE FROM productos (prod_id , prod_nombre , prod_precio ) WHERE ("id = 1");

};
BASE.inicializar = function(){ // 
    var botonCrear = document.querySelector('#btnCrearBase');
    botonCrear.setAttribute('onclick','BASE.crearBase();');
    var botonCargar = document.querySelector('#btnInsertar');
    botonCargar.setAttribute('onclick','BASE.insertar();');
    var botonConsultar = document.querySelector('#btnConsultar');
    botonConsultar.setAttribute('onclick','BASE.consultar();');
      BASE.contadorMentiroso = 0;
};
BASE.inicializar();
