
var BASE = {}; // definir un Objeto tal como new Object();, pero con JSON

BASE.elContenedor = document.querySelector('#contenedor');  // por Performance
// Para que Barazal Agustin este contento, ahora que Agustin esta medio-contento

BASE.mostrar = function(msg){ // aqui definimos un metodo para mostrar un mensaje

  // aqui obtenemos el elemento HTML contenedor
  // que podemos buscar en google con " w3 dom " 
//  var elContenedor  = document.querySelector('#contenedor');  
  
  // en este caso, teniendo el puntero al elemento, la variable, le agrego
  // dentro el contenido con u n fin de linhea de HTML
  // este atributo innerHTML, lo busco en la w3 y
  // encuentro los otros 5000 atributes que necesite
  this.elContenedor.innerHTML += msg + '<br/>';
}; // este punto y coma, es porque la funcion se definio como contenido de un atributo.

// Design Patterns: https://en.wikipedia.org/wiki/Software_design_pattern

BASE.LABASE_INSTANCE = null; // Singleton: Unica instancia del objeto Base de Datos.
BASE.crearBase = function(){ // 
    this.mostrar('Creando la Base...');
    // BASE.mostrar(" 2 == '2' " +  (2 == '2') );
    // BASE.mostrar(" 2 === '2' " +  (2 === '2') );
    
    if( BASE.LABASE_INSTANCE === null ){ // https://en.wikipedia.org/wiki/Singleton_pattern
      BASE.LABASE_INSTANCE = {}; // Existe y no es nulo!! ACA Deja de ser Null!!!!
      this.mostrar('Creando la Base...[OK]');
      alasql(" CREATE TABLE productos (prod_id INT, prod_nombre VARCHAR(200), prod_precio FLOAT);");
    }
};

  // rfc, w3, ansi, ecma estandares
  // Aca iria lo que yo quiero hacer con la Base de Datos
  // el INSERT
  // entonces:
  /*
   ANSI SQL:
   7 palabras claves: Usuario y Roles, GRANT Permisos
   
   *******************
   DDL: Modifica la estrucutrad de la tabla
   CREATE, 
   DROP, 
   ALTER
   
   DML: Manipula los datos dentro de las tablas
   INSERT, 
   UPDATE, 
   DELETE
   
   DQL: busquedas, lecturas, cuentas.
   SELECT
   *******************
   INT
   LONG
   FLOAT
   DOUBLE
   CHAR
   VARCHAR(255)
   DATE, TIME, DATETIME, TIMESTAMP
   
 */



BASE.insertar = function(){
  var laEntrada =  document.querySelector('#entrada');  
  // Aca logre cargar el dato de entrada en una variable


  // Aca quiero motrar los datos con la funcion de muestra.
  this.mostrar( laEntrada.value );
  BASE.contadorMentiroso++;
   alasql(" INSERT INTO productos (prod_id , prod_nombre , prod_precio ) VALUES ("+ BASE.contadorMentiroso +" ,'" + laEntrada.value + "', 1000);");

};

BASE.consultar = function(){
    var respuestaSQL = alasql(" SELECT * FROM productos ");
    this.mostrar( JSON.stringify( respuestaSQL ) ); // http://json.org/ Definicion del Standard
};
BASE.borrar = function(elId){
    alasql(" DELETE FROM productos WHERE prod_id = " + elId );
    this.mostrarProductos();
}
BASE.mostrarProductos = function(){ // https://github.com/janl/mustache.js
  var templateProductos = "<table><tr>{{#productos}}<td>{{prod_nombre}}</td><td>{{prod_precio}}</td><td onclick='BASE.borrar({{prod_id}});'>Clickee para Borrar Esta Linea: {{prod_id}}</td></tr>{{/productos}}</table>";
    var respuestaSQL = alasql(" SELECT * FROM productos ");
    
    var modeloProductos = {};
    modeloProductos.productos = respuestaSQL;
    
    this.mostrar( Mustache.render(templateProductos, modeloProductos) ); // http://json.org/ Definicion del Standard
  
};

BASE.inicializar = function(){ // 
    var botonCrear = document.querySelector('#btnCrearBase');
    botonCrear.setAttribute('onclick','BASE.crearBase();');
    var botonCargar = document.querySelector('#btnInsertar');
    botonCargar.setAttribute('onclick','BASE.insertar();');
    var botonConsultar = document.querySelector('#btnConsultar');
    botonConsultar.setAttribute('onclick','BASE.consultar();');
    var botonConsultarHTML = document.querySelector('#btnConsultarHTML');
    botonConsultarHTML.setAttribute('onclick','BASE.mostrarProductos();');
    BASE.contadorMentiroso = 0;
};
BASE.inicializar();
