
var BASE = {}; // definir un Objeto tal como new Object();, pero con JSON

BASE.mostrar = function(msg){ // aqui definimos un metodo para mostrar un mensaje
  var elContenedor  = document.querySelector('#contenedor');  
  elContenedor.innerHTML += msg + '<br/>';
};

BASE.cargar = function(){
  var laEntrada =  document.querySelector('#entrada');  
  BASE.mostrar( laEntrada.value );
};

